<?php
ini_set('display_errors',0);


require __DIR__.'/vendor/autoload.php';
require __DIR__.'/config/constants.php';
require __DIR__.'/config/config.php';

$container = new \Slim\Container($config);


$app = new \Slim\App($container);
include_once __DIR__."/container.php";
include_once __DIR__."/app/routes.php";

$app->run();

