<?php
namespace App\Action\Admin;

use \App\Action\Action as Action;
final class HomeAction extends Action{
  
  
  public function index($request, $response){
    
    
    $handPan = new \classes\dao\Handpan();
    
    $handPan->tipoHandPan = "d_kurd_11";
    $handPan->getHandPanConfigs();
    
    $dadosHandPan['tiposHandPan'] = $handPan->tiposHandPan;
    $dadosHandPan['tipoHandPan'] = $handPan->tipoHandPan;    
    $dadosHandPan['notas'] = $handPan->notasHandPan;
    $dadosHandPan['escala'] = $handPan->escalaHandPan;
    $dadosHandPan['binds'] = $handPan->binds;
    $dadosHandPan['srcImagem'] = $handPan->srcImagem;
    
    

    return $this->view->render($response, 'pages/home.phtml', $dadosHandPan);
  
  }

  public function getHandPan($request, $response, $args){

    $handPan = new \classes\dao\Handpan();
    $handPan->tipoHandPan = $args['tipohandpan'];
    $handPan->getHandPanConfigs();
    
    $dadosHandPan = $handPan->retornaStatusApi();    
    
    return $response->withJson($dadosHandPan);
    
  }
  
}