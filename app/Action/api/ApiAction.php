<?php
namespace App\Action\Api;

use \App\Action\Action as Action;

final class ApiAction extends Action{

  public function index($request, $response, $args){

    $handPan = new \classes\dao\Handpan();
    $handPan->tipoHandPan = $args['tipohandpan'];
    $handPan->getHandPanConfigs();
    
    $dadosHandPan = $handPan->retornaStatusApi();    
    ob_clean();
    return $response->withJson($dadosHandPan);    
  }

}
