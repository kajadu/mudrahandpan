<?php
/** 
* Classe Entidade do Boleto onde consta os atributos do mesmo.
*
* @author Luis Gabriel
* @version 0.1 
* @copyright  
* @access public  
* @example Classe Handpan_Entity
*/ 

namespace classes\entity;

class Handpan_Entity extends \classes\abstract_class\Entity
{
   
    public function __construct() {
		parent::__construct();
    }

    protected $tiposHandPan = [
      "c_harmonic_minor_13"=>[
        "label"=>"C HARMONIC MINOR 13",
        "notas"=>[
          "G3"=>["position"=>[62,47],"panner"=>0],
          "C4"=>["position"=>[80,53],"panner"=>0.25],
          "D4"=>["position"=>[80,41],"panner"=>-0.25],
          "Eb4"=>["position"=>[63,61.5],"panner"=>0.50],
          "F4"=>["position"=>[64,33.5],"panner"=>-0.50],
          "G4"=>["position"=>[44,62],"panner"=>0.50],
          "Ab4"=>["position"=>[44,32.5],"panner"=>-0.50],
          "B4"=>["position"=>[29,56.5],"panner"=>0.25],
          "C5"=>["position"=>[29.5,38],"panner"=>-0.25],
          "D5"=>["position"=>[23,46.5],"panner"=>0],
          "Eb5"=>["position"=>[35.5,47],"panner"=>0],
          "F5"=>["position"=>[47.5,54.5],"panner"=>0.15],
          "G5"=>["position"=>[47.5,40],"panner"=>-0.15]
        ]
      ],
      "c_maior_15"=>[
        "label"=>"C MAIOR 15",
        "notas"=>[
          "C4"=>["position"=>[78.5,52.5],"panner"=>0.25],
          "D4"=>["position"=>[78.5,41],"panner"=>-0.25],
          "E4"=>["position"=>[64.5,60.9],"panner"=>0.50],
          "F4"=>["position"=>[63.5,33.7],"panner"=>-0.50],
          "G4"=>["position"=>[45,62],"panner"=>0.50],
          "A4"=>["position"=>[45.5,32.7],"panner"=>-0.50],
          "B4"=>["position"=>[31,57.2],"panner"=>0.25],
          "C5"=>["position"=>[31,37.5],"panner"=>-0.25],
          "D5"=>["position"=>[24,49],"panner"=>0.10],
          "E5"=>["position"=>[33.5,44.5],"panner"=>-0.10],
          "F5"=>["position"=>[42,54],"panner"=>0.15],
          "G5"=>["position"=>[50.5,39],"panner"=>-0.15],
          "A5"=>["position"=>[60,53.5],"panner"=>0.15],
          "B5"=>["position"=>[65,44.2],"panner"=>-0.10],
          "C6"=>["position"=>[49,47],"panner"=>0]
        ]
      ],
      "d_celtic_10"=>[
        "label"=>"D CELTIC 10",
        "notas"=>[
          "D3"=>["position"=>[58.5,48],"panner"=>0],
          "A3"=>["position"=>[78,53.5],"panner"=>0.25],
          "C4"=>["position"=>[78.5,41],"panner"=>-0.25],
          "D4"=>["position"=>[59,61.5],"panner"=>0.50],
          "E4"=>["position"=>[62,33.5],"panner"=>-0.50],
          "F4"=>["position"=>[39,60.5],"panner"=>0.50],
          "G4"=>["position"=>[41.5,33.7],"panner"=>-0.50],
          "A4"=>["position"=>[25.7,52.2],"panner"=>0.25],
          "C5"=>["position"=>[28.2,40.5],"panner"=>-0.25],
          "D5"=>["position"=>[38.2,47.8],"panner"=>0]
        ]
      ],      
      "d_kurd_9"=>[
        "label"=>"D KURD 9",
        "notas"=>[
          "D3"=>["position"=>[51,47.5],"panner"=>0],
          "A3"=>["position"=>[77,53.5],"panner"=>0.25],
          "Bb3"=>["position"=>[76,40.5],"panner"=>-0.25],
          "C4"=>["position"=>[59.5,61.2],"panner"=>0.50],
          "D4"=>["position"=>[59,33.5],"panner"=>-0.50],
          "E4"=>["position"=>[39,60],"panner"=>0.50],
          "F4"=>["position"=>[39,35],"panner"=>-0.50],
          "G4"=>["position"=>[27,52.3],"panner"=>0.25],
          "A4"=>["position"=>[27,42.5],"panner"=>-0.25]
        ]
      ],
      "d_kurd_10"=>[
        "label"=>"D KURD 10",
        "notas"=>[
          "D3"=>["position"=>[57,47.8],"panner"=>0],
          "A3"=>["position"=>[78,53],"panner"=>0.25],
          "Bb3"=>["position"=>[77,40.5],"panner"=>-0.25],
          "C4"=>["position"=>[62,61.3],"panner"=>0.50],
          "D4"=>["position"=>[60,33],"panner"=>-0.50],
          "E4"=>["position"=>[42,61.5],"panner"=>0.50],
          "F4"=>["position"=>[40.5,33.7],"panner"=>-0.50],
          "G4"=>["position"=>[27.5,54.5],"panner"=>0.25],
          "A4"=>["position"=>[27.5,40.5],"panner"=>-0.25],
          "C5"=>["position"=>[37,46.7],"panner"=>0]
        ]
      ],
      "d_kurd_11"=>[
        "label"=>"D KURD 11",
        "notas"=>[
          "D3"=>["position"=>[57,47.5],"panner"=>0],
          "A3"=>["position"=>[78,53.5],"panner"=>0.25],
          "Bb3"=>["position"=>[77,40.6],"panner"=>-0.25],
          "C4"=>["position"=>[59.5,61.3],"panner"=>0.50],
          "D4"=>["position"=>[59,33.3],"panner"=>-0.50],
          "E4"=>["position"=>[38,60.8],"panner"=>0.50],
          "F4"=>["position"=>[38,34],"panner"=>-0.50],
          "G4"=>["position"=>[25,52.5],"panner"=>0.25],
          "A4"=>["position"=>[25.5,42],"panner"=>-0.25],
          "C5"=>["position"=>[39,53],"panner"=>0.15],
          "D5"=>["position"=>[39,42.4],"panner"=>-0.15]
        ]
      ],
      "d_mystic_10"=>[
        "label"=>"D MYSTIC 10",
        "notas"=>[
          "D3"=>["position"=>[57,47.8],"panner"=>0],
          "A3"=>["position"=>[78,53],"panner"=>0.25],
          "Bb3"=>["position"=>[77,40.5],"panner"=>-0.25],
          "D4"=>["position"=>[62,61.3],"panner"=>0.50],
          "E4"=>["position"=>[60,33],"panner"=>-0.50],
          "F4"=>["position"=>[42,61.5],"panner"=>0.50],
          "G4"=>["position"=>[40.5,33.7],"panner"=>-0.50],
          "A4"=>["position"=>[27.5,54.5],"panner"=>0.25],
          "C5"=>["position"=>[27.5,40.5],"panner"=>-0.25],
          "D5"=>["position"=>[37,46.7],"panner"=>0]
        ]
      ],
      "e_kurd_12"=>[
        "label"=>"E KURD 12",
        "notas"=>[
          "E3"=>["position"=>[57.5,47.5],"panner"=>0],
          "B3"=>["position"=>[78.5,53.3],"panner"=>0.25],
          "C4"=>["position"=>[78,41],"panner"=>-0.25],
          "D4"=>["position"=>[63,61.5],"panner"=>0.50],
          "E4"=>["position"=>[62,33.5],"panner"=>-0.50],
          "Fs4"=>["position"=>[43,62],"panner"=>0.50],
          "G4"=>["position"=>[43,32.6],"panner"=>-0.50],
          "A4"=>["position"=>[28,56],"panner"=>0.25],
          "B4"=>["position"=>[28,38.5],"panner"=>-0.25],
          "D5"=>["position"=>[23,47.5],"panner"=>0],
          "E5"=>["position"=>[40.8,41.2],"panner"=>-0.15],
          "Fs5"=>["position"=>[40,53.3],"panner"=>0.15]
        ]
      ],
      "e_hijaz_10"=>[
        "label"=>"E HIJAZ 10",
        "notas"=>[
          "E3"=>["position"=>[52,48.5],"panner"=>0],
          "A3"=>["position"=>[78,53.5],"panner"=>0.25],
          "B3"=>["position"=>[78,42],"panner"=>-0.25],
          "C4"=>["position"=>[63,61.2],"panner"=>0.50],
          "Ds4"=>["position"=>[63,34.5],"panner"=>-0.50],
          "E4"=>["position"=>[44,61.5],"panner"=>0.50],
          "Fs4"=>["position"=>[45,34],"panner"=>-0.50],
          "G4"=>["position"=>[29.5,56],"panner"=>0.25],
          "B4"=>["position"=>[30.5,38.7],"panner"=>0.25],
          "C5"=>["position"=>[25,47.5],"panner"=>0]
        ]        
      ]
    ];

    protected $tipoHandPan;

    protected $notasHandPan = [];

    protected $escalaHandPan = "";

    protected $srcImagem = "";    

    protected $boardKeys = [
      "Digit1",
      "Digit2",
      "Digit3",
      "Digit4",
      "Digit5",
      "Digit6",
      "Digit7",
      "Digit8",
      "Digit9",
      "Digit0",
      "KeyQ",
      "KeyW",
      "KeyE",
      "KeyR",
      "KeyT",
      "KeyY",
      "KeyU",
      "KeyI",
      "KeyO",
      "KeyP"
    ];

    protected $binds = [];

    protected $panner = [];

    protected $tipoArquivoAudio = "";

}
