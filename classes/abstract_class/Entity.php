<?php

namespace classes\abstract_class;


abstract class Entity {
    
     protected $tipoHandPan = "";
     
    
     public function __construct() {
		// include_once "/var/www/html/";
		// $this->conn = Zend_Conn();	
	}
 
        /**
     * Metodo mágico Retorna o conteudo do valor setado
     *
     * @param Mixed $valor da propriedade da classe
     *
     * @return Mixed retorna o conteudo referente a propriedade solicitada
     */
     public function __get($valor)
     {         
          
          return $this->$valor;
     }

    /**
     * Metodo mágico Adiciona o conteudo a propriedade da classe
     *
     * @param Mixed $propriedade da propriedade da classe
     * @param Mixed $valor       da propriedade da classe
     *
     * @return Mixed retorna o conteudo referente a propriedade solicitada
     */
     public function __set($propriedade,$valor)
     {
          $this->$propriedade = $valor;
          
     }

     public function retornaStatusApi(){
          $retorno = [];       

          $retorno['tipoHandPan'] = $this->tipoHandPan;    
          $retorno['notas'] = $this->notasHandPan;
          $retorno['escala'] = $this->escalaHandPan;
          $retorno['binds'] = $this->binds;
          $retorno['srcImagem'] = $this->srcImagem;            
          
          return $retorno;
      }
     
}
