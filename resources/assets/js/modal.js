$(function(){
    $(".container-fluid").addClass("");
    $(".container-fluid").fadeTo("fast",0.70);
    $(".modal").show();
    
    $(".text_pt_br").hide();

    $(".language").click(function (){
       var language = $(this).attr("id");       
       $(".terms").hide();
       $(".text_"+language).show();
    });

    $("#ok").click(function (){
        $(".container-fluid").fadeTo("fast",1);
        $(".modal").hide();
    });

});