$(function(){
    var audio = {};
    var notaId;
    var panner = {
        "E3": 0,
        "A3": 0.25,
        "B3": -0.25,
        "C4": 0.50,
        "Ds4": -0.50,
        "E4": 0.50,
        "Fs4": -0.50,
        "G4": 0.25,
        "B4": -0.25,
        "C5": 0,
    };
    var notas = ["E3","A3","B3","C4","Ds4","E4","Fs4","G4","B4","C5"];
    
    $(".nota").click(function (){
        var nota = $(this).attr("nota");
        var src = "files/notas/hijaz10/"+nota+".mp3";
        $("#nota-"+nota).removeClass("text-white");
        $("#nota-"+nota).addClass("text-danger");
        
        var sound = new Pz.Sound({
            source: "file",
            options: { path: src }
        },function (){
            var compressor = new Pz.Effects.Compressor({
                threshold: 0,        
                ratio: 20
            });
            var stereoPanner = new Pz.Effects.StereoPanner({
                pan: panner[nota]
            });
            playSound(sound,compressor, stereoPanner);
            $("#nota-"+nota).removeClass("text-danger");
            $("#nota-"+nota).addClass("text-white");
        });
        // console.log(audio);
        // return false;        
    });    
    
    //Carregando as notas...
    notas.forEach(nota => {
        audio[nota] = new Pz.Sound({
            source: "file",
            options: { path: "files/notas/hijaz10/"+nota+".mp3"}     
        },function (){
            var compressor = new Pz.Effects.Compressor({
                threshold: 0,        
                ratio: 20
            });
            var panner = new Pz.Effects.StereoPanner({
                pan: panner[nota]
            });
        });
        audio.E3.addEffect(compressor);
        audio.E3.addEffect(panner);
    });

    playSound(audio.E3);
    $("#nota-"+notas["E3"]).removeClass("text-danger");
    $("#nota-"+notas["E3"]).addClass("text-white");
    
        
    
    
    $("body").keypress(function (event){
        //console.log(event.originalEvent.key);
        //console.log(event.originalEvent.code);
        $("#nota-"+notas[event.originalEvent.code]).removeClass("text-white");
        $("#nota-"+notas[event.originalEvent.code]).addClass("text-danger");

        audio[event.originalEvent.code] = new Pz.Sound({
            source: "file",
            options: { path: "files/notas/hijaz10/"+notas[event.originalEvent.code]+".mp3"}     
        },function (){
            var compressor = new Pz.Effects.Compressor({
                threshold: 0,        
                ratio: 20
            });
            var stereoPanner = new Pz.Effects.StereoPanner({
                pan: panner[notas[event.originalEvent.code]]
            });
            playSound(audio[event.originalEvent.code],compressor, stereoPanner);
            $("#nota-"+notas[event.originalEvent.code]).removeClass("text-danger");
            $("#nota-"+notas[event.originalEvent.code]).addClass("text-white");
        });        
    });

    function playSound(sound) {
        sound.attack = 0.00;
        sound.release = 0.00;        
        sound.play();        
    }
});